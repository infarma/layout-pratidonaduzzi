package retornoDosPedidosDeVendas

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DadosComplementares struct {
	TipoDoRegistro        int64 	`json:"TipoDoRegistro"`
	CnpjOperadorLogistico string	`json:"CnpjOperadorLogistico"`
	SemUtilizacao0        string	`json:"SemUtilizacao0"`
	Data                  string	`json:"Data"`
	SemUtilizacao1        string	`json:"SemUtilizacao1"`
}

func (d *DadosComplementares) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDadosComplementares

	err = posicaoParaValor.ReturnByType(&d.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CnpjOperadorLogistico, "CnpjOperadorLogistico")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao0, "SemUtilizacao0")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Data, "Data")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao1, "SemUtilizacao1")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDadosComplementares = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 1, 0},
	"CnpjOperadorLogistico":                      {1, 15, 0},
	"SemUtilizacao0":                      {15, 17, 0},
	"Data":                      {17, 25, 0},
	"SemUtilizacao1":                      {25, 51, 0},
}