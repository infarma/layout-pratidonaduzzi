package retornoDosPedidosDeVendas

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	TipoDoRegistro                   int64 	`json:"TipoDoRegistro"`
	QuantidadeDeUnidadesAtendidas    int64 	`json:"QuantidadeDeUnidadesAtendidas"`
	QuantidadeDeUnidadesNaoAtendidas int64 	`json:"QuantidadeDeUnidadesNaoAtendidas"`
	QuantidadeDeItensDoArquivo       int64 	`json:"QuantidadeDeItensDoArquivo"`
	SemUtilizacao                    string	`json:"SemUtilizacao"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeDeUnidadesAtendidas, "QuantidadeDeUnidadesAtendidas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeDeUnidadesNaoAtendidas, "QuantidadeDeUnidadesNaoAtendidas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeDeItensDoArquivo, "QuantidadeDeItensDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 1, 0},
	"QuantidadeDeUnidadesAtendidas":                      {1, 18, 0},
	"QuantidadeDeUnidadesNaoAtendidas":                      {18, 35, 0},
	"QuantidadeDeItensDoArquivo":                      {35, 39, 0},
	"SemUtilizacao":                      {39, 51, 0},
}