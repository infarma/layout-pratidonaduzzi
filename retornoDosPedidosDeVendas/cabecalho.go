package retornoDosPedidosDeVendas

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoDoRegistro          int64 	`json:"TipoDoRegistro"`
	CnpjDoCliente           string	`json:"CnpjDoCliente"`
	SemUtilizacao0          string	`json:"SemUtilizacao0"`
	PedidoGeradoPeloCliente string	`json:"PedidoGeradoPeloCliente"`
	SemUtilizacao1          string	`json:"SemUtilizacao1"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjDoCliente, "CnpjDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.SemUtilizacao0, "SemUtilizacao0")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PedidoGeradoPeloCliente, "PedidoGeradoPeloCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.SemUtilizacao1, "SemUtilizacao1")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 1, 0},
	"CnpjDoCliente":                      {1, 15, 0},
	"SemUtilizacao0":                      {15, 16, 0},
	"PedidoGeradoPeloCliente":                      {16, 25, 0},
	"SemUtilizacao1":                      {25, 51, 0},
}