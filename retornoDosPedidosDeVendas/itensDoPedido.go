package retornoDosPedidosDeVendas

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type ItensDoPedido struct {
	TipoDoRegistro          int64	`json:"TipoDoRegistro"`
	EanDoProduto            int64	`json:"EanDoProduto"`
	QuantidadeAtendida      int64	`json:"QuantidadeAtendida"`
	QuantidadeNaoAtendidade int64	`json:"QuantidadeNaoAtendidade"`
	Motivo                  int64	`json:"Motivo"`
	Retorno                 int64	`json:"Retorno"`
}

func (i *ItensDoPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItensDoPedido

	err = posicaoParaValor.ReturnByType(&i.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.EanDoProduto, "EanDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeAtendida, "QuantidadeAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeNaoAtendidade, "QuantidadeNaoAtendidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Motivo, "Motivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Retorno, "Retorno")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItensDoPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 1, 0},
	"EanDoProduto":                      {1, 14, 0},
	"QuantidadeAtendida":                      {14, 31, 0},
	"QuantidadeNaoAtendidade":                      {31, 48, 0},
	"Motivo":                      {48, 50, 0},
	"Retorno":                      {50, 51, 0},
}