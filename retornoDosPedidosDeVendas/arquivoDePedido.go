package retornoDosPedidosDeVendas

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Cabecalho           Cabecalho           `json:"Cabecalho"`
	DadosComplementares DadosComplementares `json:"DadosComplementares"`
	ItensDoPedido       []ItensDoPedido     `json:"ItensDoPedido"`
	Rodape              Rodape              `json:"Rodape"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		var indexItem int32
		if identificador == "01" {
			err = arquivo.Cabecalho.ComposeStruct(string(runes))
		} else if identificador == "02" {
			err = arquivo.DadosComplementares.ComposeStruct(string(runes))
		} else if identificador == "03" {
			err = arquivo.ItensDoPedido[indexItem].ComposeStruct(string(runes))
		} else if identificador == "04" {
			err = arquivo.Rodape.ComposeStruct(string(runes))
		}

		indexItem++
	}
	return arquivo, err
}
