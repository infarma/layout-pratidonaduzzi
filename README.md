Layout de Envio dos Pedidos de Vendas

    Cabeçalho do Pedido:
    
        gerador_de_layouts envioDoPedidoDeVendas cabecalhoDoPedido TipoDoRegistro:int64:0:1 OrigemDoPedido:string:1:2 NumeroDoPedidoCliente:string:2:11 CnpjParaFaturamento:string:11:25 NomeDoCliente:string:25:75 CodigoDoVendedor:string:75:81 NomeDoVendedor:string:81:111 DataDeEmissao:string:111:119 DataDeEntrega:string:119:127 TelefoneCliente:string:127:142 UfCliente:string:142:144 MunicipioCliente:string:144:184 EnderecoCliente:string:184:234 CepCliente:int64:234:242 CondPgto:string:242:247 DescricaoCondPgto:string:247:282 FormaDePgto:string:282:302 CnpjOperadorLogistico:string:302:316 Observacoes:string:316:402 NumeroDoPedidoDoCliente:string:402:417 
    
    Item do Pedido: 
    
        gerador_de_layouts envioDoPedidoDeVendas itensDoPedido TipoDoRegistro:int64:0:1 CodigoEanDoProduto:string:1:14 CodigoDoProduto:int64:14:20 DescricaoDoProduto:string:20:60 Branco0:string:60:61 UnidadeMedidaComercializada:string:61:64 Branco1:string:64:65 Quantidade:string:65:77 Branco2:string:77:78 ValorUnitario:float64:78:90:5 Branco3:string:90:91 ValorTotalDoPedido:float64:91:103:2 Branco4:string:103:104 PrecoFabrica:float64:104:116:5 Branco5:string:116:117 PercentualDeDesconto:float64:117:129:2 Branco6:string:129:130 NumeroDoPedidoCliente:string:130:139 CodigoEanAdicionalDoProduto:string:139:152 SemUtilizacao:string:152:417
    
    Rodapé:
    
        gerador_de_layouts envioDoPedidoDeVendas rodape TipoDoRegistro:int64:0:1 QuantidadeDeLinhas:int64:1:6 SemUtilizacao:string:6:417


Layout de Retorno dos Pedidos de Vendas

    Cabecalho:
        
        gerador_de_layouts retornoDosPedidosDeVendas cabecalho TipoDoRegistro:int64:0:1 CnpjDoCliente:string:1:15 SemUtilizacao0:string:15:16 PedidoGeradoPeloCliente:string:16:25 SemUtilizacao1:string:25:51
        
        
    Dados Complementares:
    
        gerador_de_layouts retornoDosPedidosDeVendas dadosComplementares TipoDoRegistro:int64:0:1 CnpjOperadorLogistico:string:1:15 SemUtilizacao0:string:15:17 Data:string:17:25 SemUtilizacao1:string:25:51 
        
    Itens do Pedido:
    
        gerador_de_layouts retornoDosPedidosDeVendas itensDoPedido TipoDoRegistro:int64:0:1 EanDoProduto:int64:1:14 QuantidadeAtendida:int64:14:31 QuantidadeNaoAtendidade:int64:31:48 Motivo:int64:48:50 Retorno:int64:50:51
        
    Rodape
    
        gerador_de_layouts retornoDosPedidosDeVendas rodape TipoDoRegistro:int64:0:1 QuantidadeDeUnidadesAtendidas:int64:1:18 QuantidadeDeUnidadesNaoAtendidas:int64:18:35 QuantidadeDeItensDoArquivo:int64:35:39 SemUtilizacao:string:39:51
        
Layout do Envio do Estoque por Lote

    Registro:
    
        gerador_de_layouts envioDoEstoquePorLote registro CnpjOperadorLogistico:string:0:14 CodigoEanDoProduto:string:14:27 Lote:string:27:37 Estoque:int64:37:54 Data:string:54:62
        
Layout do Mapa de Vendas

    Cadastro de Clientes:
    
        gerador_de_layouts mapaDeVendas CadastroDeClientes TipoDoRegistro:string:0:1 RazaoSocialDoCliente:string:1:41 TipoDeInscricao:string:41:42 NomeFantasiaDoCliente:string:42:82 EnderecoDoCliente:string:82:122 MunicipioDoCliente:string:122:157 EstadoUFDoCliente:string:157:159 BairroDoCliente:string:159:189 CepDoCliente:string:189:197 ComplementoDoEndereco:string:197:247 DddDoTelefoneDoCliente:string:247:250 TelefoneDoCliente:string:250:265 DddDoFaxDoCliente:string:265:268 FaxDoCliente:string:268:283 PessoaParaContatoNoCliente:string:283:298 CnpjOuCpfDoCliente:string:298:312 InscricaoEstadualDoCliente:string:312:330 EmailDoCliente:string:330:375 EnderecoDeCobrancaDoCliente:string:375:415 BairroDeCobrancaDoCliente:string:415:445 MunicipioDeCobrancaDoCliente:string:445:475 CepDeCobrancaDoCliente:string:475:483 UfDeCobrancaDoCliente:string:483:485 EnderecoDeEntregaDoCliente:string:485:525 BairroDeEntregaDoCliente:string:525:545 MunicipioDeEntregaDoCliente:string:545:575 CepDeEntregaDoCliente:string:575:583 UfDeEntregaDoCliente:string:583:585 Brancos:string:585:599 FimDoRegistro:string:599:600
        
    Vendas Realizadas:
    
        gerador_de_layouts mapaDeVendas VendasRealizadas TipoDoRegistro:string:0:1 CodigoDoVendedor:string:1:7 Branco0:string:7:8 NumeroDaNotaFiscalDeVenda0:string:8:14 Branco1:string:14:15 SerieDaNotaFiscalDeVenda:string:15:18 Branco2:string:18:19 DataDaNotaFiscalDeVenda:string:19:27 Branco3:string:27:28 CnpjDoCliente:string:28:42 Branco4:string:42:43 CodigoDoProduto:string:43:49 Branco5:string:49:50 QuantidadeVendida:int64:50:67 Branco6:string:67:68 ValorUnitarioDoItem:int64:68:85 Branco7:string:85:86 ValorTotalDoItem:int64:86:103 Branco8:string:103:104 NumeroDoPedidoOriginal:string:104:113 Branco9:string:113:114 CodigoDeBarrasDoProduto:string:114:127 Branco10:string:127:128 NumeroDaNotaFiscalDeVenda1:string:128:137 Branco11:string:137:599 FimDeRegistro:string:599:600
        
    Decolução de Produtos:
        
        gerador_de_layouts mapaDeVendas DevolucaoDeProdutos TipoDoRegistro:string:0:1 CodigoDoVendedor:string:1:7 Branco0:string:7:8 NumeroDaNotaFiscal0:string:8:14 Branco1:string:14:15 SerieDaNotaFiscal:string:15:18 Branco2:string:18:19 DataDaNotaFiscal:string:19:27 Branco3:string:27:28 CnpjDoCliente:string:28:42 Branco4:string:42:43 CodigoDoproduto:string:43:49 Branco5:string:49:50 Quantidade:int64:50:67 Branco6:string:67:68 ValorUnitario:int64:68:85 Branco7:int64:85:86 ValorTotal:int64:86:103 Brancos8:string:103:104 CodigoDeBarrasDoProduto:string:104:117 Brancos9:string:117:118 NotaFiscalDeOrigem:string:118:124 Brancos10:string:124:125 SerieDaNotaFiscalDeOrigem:string:125:128 Brancos11:string:128:129 NumeroDaNotaFiscalDeVnda:string:129:138 Brancos12:string:138:139 NumeroDaNotaFiscalDeOrigem:string:139:148 Brancos13:string:148:599 FimDeRegistro:string:599:600 