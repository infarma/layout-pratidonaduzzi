package layout_pratidonaduzzi

import (
	"bitbucket.org/infarma/layout-pratidonaduzzi/envioDoPedidoDeVendas"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (envioDoPedidoDeVendas.ArquivoDePedido, error) {
	return envioDoPedidoDeVendas.GetStruct(fileHandle)
}