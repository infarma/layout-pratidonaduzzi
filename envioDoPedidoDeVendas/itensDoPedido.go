package envioDoPedidoDeVendas

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type ItensDoPedido struct {
	TipoDoRegistro              int64   `json:"TipoDoRegistro"`
	CodigoEanDoProduto          string  `json:"CodigoEanDoProduto"`
	CodigoDoProduto             int64   `json:"CodigoDoProduto"`
	DescricaoDoProduto          string  `json:"DescricaoDoProduto"`
	Branco0                     string  `json:"Branco0"`
	UnidadeMedidaComercializada string  `json:"UnidadeMedidaComercializada"`
	Branco1                     string  `json:"Branco1"`
	Quantidade                  string  `json:"Quantidade"`
	Branco2                     string  `json:"Branco2"`
	ValorUnitario               float64 `json:"ValorUnitario"`
	Branco3                     string  `json:"Branco3"`
	ValorTotalDoPedido          float64 `json:"ValorTotalDoPedido"`
	Branco4                     string  `json:"Branco4"`
	PrecoFabrica                float64 `json:"PrecoFabrica"`
	Branco5                     string  `json:"Branco5"`
	PercentualDeDesconto        float64 `json:"PercentualDeDesconto"`
	Branco6                     string  `json:"Branco6"`
	NumeroDoPedidoCliente       string  `json:"NumeroDoPedidoCliente"`
	CodigoEanAdicionalDoProduto string  `json:"CodigoEanAdicionalDoProduto"`
	SemUtilizacao               string  `json:"SemUtilizacao"`
}

func (i *ItensDoPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItensDoPedido

	err = posicaoParaValor.ReturnByType(&i.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoEanDoProduto, "CodigoEanDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoDoProduto, "CodigoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.DescricaoDoProduto, "DescricaoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Branco0, "Branco0")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.UnidadeMedidaComercializada, "UnidadeMedidaComercializada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Branco1, "Branco1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Branco2, "Branco2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorUnitario, "ValorUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Branco3, "Branco3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorTotalDoPedido, "ValorTotalDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Branco4, "Branco4")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrecoFabrica, "PrecoFabrica")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Branco5, "Branco5")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PercentualDeDesconto, "PercentualDeDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Branco6, "Branco6")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroDoPedidoCliente, "NumeroDoPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoEanAdicionalDoProduto, "CodigoEanAdicionalDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItensDoPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":              {0, 1, 0},
	"CodigoEanDoProduto":          {1, 14, 0},
	"CodigoDoProduto":             {14, 20, 0},
	"DescricaoDoProduto":          {20, 60, 0},
	"Branco0":                     {60, 61, 0},
	"UnidadeMedidaComercializada": {61, 64, 0},
	"Branco1":                     {64, 65, 0},
	"Quantidade":                  {65, 77, 0},
	"Branco2":                     {77, 78, 0},
	"ValorUnitario":               {78, 90, 5},
	"Branco3":                     {90, 91, 0},
	"ValorTotalDoPedido":          {91, 103, 2},
	"Branco4":                     {103, 104, 0},
	"PrecoFabrica":                {104, 116, 5},
	"Branco5":                     {116, 117, 0},
	"PercentualDeDesconto":        {117, 129, 2},
	"Branco6":                     {129, 130, 0},
	"NumeroDoPedidoCliente":       {130, 139, 0},
	"CodigoEanAdicionalDoProduto": {139, 152, 0},
	"SemUtilizacao":               {152, 417, 0},
}
