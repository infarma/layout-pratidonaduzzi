package envioDoPedidoDeVendas

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CabecalhoDoPedido struct {
	TipoDoRegistro          int64  `json:"TipoDoRegistro"`
	OrigemDoPedido          string `json:"OrigemDoPedido"`
	NumeroDoPedidoCliente   string `json:"NumeroDoPedidoCliente"`
	CnpjParaFaturamento     string `json:"CnpjParaFaturamento"`
	NomeDoCliente           string `json:"NomeDoCliente"`
	CodigoDoVendedor        string `json:"CodigoDoVendedor"`
	NomeDoVendedor          string `json:"NomeDoVendedor"`
	DataDeEmissao           string `json:"DataDeEmissao"`
	DataDeEntrega           string `json:"DataDeEntrega"`
	TelefoneCliente         string `json:"TelefoneCliente"`
	UfCliente               string `json:"UfCliente"`
	MunicipioCliente        string `json:"MunicipioCliente"`
	EnderecoCliente         string `json:"EnderecoCliente"`
	CepCliente              int64  `json:"CepCliente"`
	CondPgto                string `json:"CondPgto"`
	DescricaoCondPgto       string `json:"DescricaoCondPgto"`
	FormaDePgto             string `json:"FormaDePgto"`
	CnpjOperadorLogistico   string `json:"CnpjOperadorLogistico"`
	Observacoes             string `json:"Observacoes"`
	NumeroDoPedidoDoCliente string `json:"NumeroDoPedidoDoCliente"`
}

func (c *CabecalhoDoPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalhoDoPedido

	err = posicaoParaValor.ReturnByType(&c.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.OrigemDoPedido, "OrigemDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDoPedidoCliente, "NumeroDoPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjParaFaturamento, "CnpjParaFaturamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NomeDoCliente, "NomeDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoDoVendedor, "CodigoDoVendedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NomeDoVendedor, "NomeDoVendedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataDeEmissao, "DataDeEmissao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataDeEntrega, "DataDeEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TelefoneCliente, "TelefoneCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.UfCliente, "UfCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.MunicipioCliente, "MunicipioCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EnderecoCliente, "EnderecoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CepCliente, "CepCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CondPgto, "CondPgto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DescricaoCondPgto, "DescricaoCondPgto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.FormaDePgto, "FormaDePgto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjOperadorLogistico, "CnpjOperadorLogistico")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Observacoes, "Observacoes")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDoPedidoDoCliente, "NumeroDoPedidoDoCliente")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalhoDoPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":          {0, 1, 0},
	"OrigemDoPedido":          {1, 2, 0},
	"NumeroDoPedidoCliente":   {2, 11, 0},
	"CnpjParaFaturamento":     {11, 25, 0},
	"NomeDoCliente":           {25, 75, 0},
	"CodigoDoVendedor":        {75, 81, 0},
	"NomeDoVendedor":          {81, 111, 0},
	"DataDeEmissao":           {111, 119, 0},
	"DataDeEntrega":           {119, 127, 0},
	"TelefoneCliente":         {127, 142, 0},
	"UfCliente":               {142, 144, 0},
	"MunicipioCliente":        {144, 184, 0},
	"EnderecoCliente":         {184, 234, 0},
	"CepCliente":              {234, 242, 0},
	"CondPgto":                {242, 247, 0},
	"DescricaoCondPgto":       {247, 282, 0},
	"FormaDePgto":             {282, 302, 0},
	"CnpjOperadorLogistico":   {302, 316, 0},
	"Observacoes":             {316, 402, 0},
	"NumeroDoPedidoDoCliente": {402, 417, 0},
}
