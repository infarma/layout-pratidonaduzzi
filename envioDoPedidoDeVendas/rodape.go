package envioDoPedidoDeVendas

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	TipoDoRegistro     int64  `json:"TipoDoRegistro"`
	QuantidadeDeLinhas int64  `json:"QuantidadeDeLinhas"`
	SemUtilizacao      string `json:"SemUtilizacao"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeDeLinhas, "QuantidadeDeLinhas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":     {0, 1, 0},
	"QuantidadeDeLinhas": {1, 6, 0},
	"SemUtilizacao":      {6, 417, 0},
}
