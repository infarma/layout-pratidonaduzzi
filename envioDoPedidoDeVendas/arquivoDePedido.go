package envioDoPedidoDeVendas

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	CabecalhoDoPedido CabecalhoDoPedido `json:"CabecalhoDoPedido"`
	ItensDoPedido     []ItensDoPedido   `json:"ItensDoPedido"`
	Rodape            Rodape            `json:"Rodape"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		var indexItem int32
		if identificador == "01" {
			err = arquivo.CabecalhoDoPedido.ComposeStruct(string(runes))
		} else if identificador == "02" {
			err = arquivo.ItensDoPedido[indexItem].ComposeStruct(string(runes))
			indexItem++
		} else if identificador == "09" {
			err = arquivo.Rodape.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
