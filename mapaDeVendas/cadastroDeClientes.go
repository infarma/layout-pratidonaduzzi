package mapaDeVendas

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CadastroDeClientes struct {
	TipoDoRegistro               string `json:"TipoDoRegistro"`
	RazaoSocialDoCliente         string `json:"RazaoSocialDoCliente"`
	TipoDeInscricao              string `json:"TipoDeInscricao"`
	NomeFantasiaDoCliente        string `json:"NomeFantasiaDoCliente"`
	EnderecoDoCliente            string `json:"EnderecoDoCliente"`
	MunicipioDoCliente           string `json:"MunicipioDoCliente"`
	EstadoUFDoCliente            string `json:"EstadoUFDoCliente"`
	BairroDoCliente              string `json:"BairroDoCliente"`
	CepDoCliente                 string `json:"CepDoCliente"`
	ComplementoDoEndereco        string `json:"ComplementoDoEndereco"`
	DddDoTelefoneDoCliente       string `json:"DddDoTelefoneDoCliente"`
	TelefoneDoCliente            string `json:"TelefoneDoCliente"`
	DddDoFaxDoCliente            string `json:"DddDoFaxDoCliente"`
	FaxDoCliente                 string `json:"FaxDoCliente"`
	PessoaParaContatoNoCliente   string `json:"PessoaParaContatoNoCliente"`
	CnpjOuCpfDoCliente           string `json:"CnpjOuCpfDoCliente"`
	InscricaoEstadualDoCliente   string `json:"InscricaoEstadualDoCliente"`
	EmailDoCliente               string `json:"EmailDoCliente"`
	EnderecoDeCobrancaDoCliente  string `json:"EnderecoDeCobrancaDoCliente"`
	BairroDeCobrancaDoCliente    string `json:"BairroDeCobrancaDoCliente"`
	MunicipioDeCobrancaDoCliente string `json:"MunicipioDeCobrancaDoCliente"`
	CepDeCobrancaDoCliente       string `json:"CepDeCobrancaDoCliente"`
	UfDeCobrancaDoCliente        string `json:"UfDeCobrancaDoCliente"`
	EnderecoDeEntregaDoCliente   string `json:"EnderecoDeEntregaDoCliente"`
	BairroDeEntregaDoCliente     string `json:"BairroDeEntregaDoCliente"`
	MunicipioDeEntregaDoCliente  string `json:"MunicipioDeEntregaDoCliente"`
	CepDeEntregaDoCliente        string `json:"CepDeEntregaDoCliente"`
	UfDeEntregaDoCliente         string `json:"UfDeEntregaDoCliente"`
	Brancos                      string `json:"Brancos"`
	FimDoRegistro                string `json:"FimDoRegistro"`
}

func (c *CadastroDeClientes) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCadastroDeClientes

	err = posicaoParaValor.ReturnByType(&c.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.RazaoSocialDoCliente, "RazaoSocialDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoDeInscricao, "TipoDeInscricao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NomeFantasiaDoCliente, "NomeFantasiaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EnderecoDoCliente, "EnderecoDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.MunicipioDoCliente, "MunicipioDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EstadoUFDoCliente, "EstadoUFDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.BairroDoCliente, "BairroDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CepDoCliente, "CepDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ComplementoDoEndereco, "ComplementoDoEndereco")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DddDoTelefoneDoCliente, "DddDoTelefoneDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TelefoneDoCliente, "TelefoneDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DddDoFaxDoCliente, "DddDoFaxDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.FaxDoCliente, "FaxDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PessoaParaContatoNoCliente, "PessoaParaContatoNoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjOuCpfDoCliente, "CnpjOuCpfDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.InscricaoEstadualDoCliente, "InscricaoEstadualDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EmailDoCliente, "EmailDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EnderecoDeCobrancaDoCliente, "EnderecoDeCobrancaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.BairroDeCobrancaDoCliente, "BairroDeCobrancaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.MunicipioDeCobrancaDoCliente, "MunicipioDeCobrancaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CepDeCobrancaDoCliente, "CepDeCobrancaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.UfDeCobrancaDoCliente, "UfDeCobrancaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EnderecoDeEntregaDoCliente, "EnderecoDeEntregaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.BairroDeEntregaDoCliente, "BairroDeEntregaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.MunicipioDeEntregaDoCliente, "MunicipioDeEntregaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CepDeEntregaDoCliente, "CepDeEntregaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.UfDeEntregaDoCliente, "UfDeEntregaDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Brancos, "Brancos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.FimDoRegistro, "FimDoRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCadastroDeClientes = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":               {0, 1, 0},
	"RazaoSocialDoCliente":         {1, 41, 0},
	"TipoDeInscricao":              {41, 42, 0},
	"NomeFantasiaDoCliente":        {42, 82, 0},
	"EnderecoDoCliente":            {82, 122, 0},
	"MunicipioDoCliente":           {122, 157, 0},
	"EstadoUFDoCliente":            {157, 159, 0},
	"BairroDoCliente":              {159, 189, 0},
	"CepDoCliente":                 {189, 197, 0},
	"ComplementoDoEndereco":        {197, 247, 0},
	"DddDoTelefoneDoCliente":       {247, 250, 0},
	"TelefoneDoCliente":            {250, 265, 0},
	"DddDoFaxDoCliente":            {265, 268, 0},
	"FaxDoCliente":                 {268, 283, 0},
	"PessoaParaContatoNoCliente":   {283, 298, 0},
	"CnpjOuCpfDoCliente":           {298, 312, 0},
	"InscricaoEstadualDoCliente":   {312, 330, 0},
	"EmailDoCliente":               {330, 375, 0},
	"EnderecoDeCobrancaDoCliente":  {375, 415, 0},
	"BairroDeCobrancaDoCliente":    {415, 445, 0},
	"MunicipioDeCobrancaDoCliente": {445, 475, 0},
	"CepDeCobrancaDoCliente":       {475, 483, 0},
	"UfDeCobrancaDoCliente":        {483, 485, 0},
	"EnderecoDeEntregaDoCliente":   {485, 525, 0},
	"BairroDeEntregaDoCliente":     {525, 545, 0},
	"MunicipioDeEntregaDoCliente":  {545, 575, 0},
	"CepDeEntregaDoCliente":        {575, 583, 0},
	"UfDeEntregaDoCliente":         {583, 585, 0},
	"Brancos":                      {585, 599, 0},
	"FimDoRegistro":                {599, 600, 0},
}
