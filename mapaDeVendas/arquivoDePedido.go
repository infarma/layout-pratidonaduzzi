package mapaDeVendas

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	CadastroDeClientes  CadastroDeClientes    `json:"CadastroDeClientes"`
	DevolucaoDeProdutos []DevolucaoDeProdutos `json:"DevolucaoDeProdutos"`
	VendasRealizadas    []VendasRealizadas    `json:"VendasRealizadsa"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		var indexItem int32
		if identificador == "01" {
			err = arquivo.CadastroDeClientes.ComposeStruct(string(runes))
		} else if identificador == "02" {
			err = arquivo.VendasRealizadas[indexItem].ComposeStruct(string(runes))
		} else if identificador == "03" {
			err = arquivo.DevolucaoDeProdutos[indexItem].ComposeStruct(string(runes))
		}

		indexItem++
	}
	return arquivo, err
}
