package mapaDeVendas

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DevolucaoDeProdutos struct {
	TipoDoRegistro             string `json:"TipoDoRegistro"`
	CodigoDoVendedor           string `json:"CodigoDoVendedor"`
	Branco0                    string `json:"Branco0"`
	NumeroDaNotaFiscal0        string `json:"NumeroDaNotaFiscal0"`
	Branco1                    string `json:"Branco1"`
	SerieDaNotaFiscal          string `json:"SerieDaNotaFiscal"`
	Branco2                    string `json:"Branco2"`
	DataDaNotaFiscal           string `json:"DataDaNotaFiscal"`
	Branco3                    string `json:"Branco3"`
	CnpjDoCliente              string `json:"CnpjDoCliente"`
	Branco4                    string `json:"Branco4"`
	CodigoDoproduto            string `json:"CodigoDoproduto"`
	Branco5                    string `json:"Branco5"`
	Quantidade                 int64  `json:"Quantidade"`
	Branco6                    string `json:"Branco6"`
	ValorUnitario              int64  `json:"ValorUnitario"`
	Branco7                    int64  `json:"Branco7"`
	ValorTotal                 int64  `json:"ValorTotal"`
	Brancos8                   string `json:"Brancos8"`
	CodigoDeBarrasDoProduto    string `json:"CodigoDeBarrasDoProduto"`
	Brancos9                   string `json:"Brancos9"`
	NotaFiscalDeOrigem         string `json:"NotaFiscalDeOrigem"`
	Brancos10                  string `json:"Brancos10"`
	SerieDaNotaFiscalDeOrigem  string `json:"SerieDaNotaFiscalDeOrigem"`
	Brancos11                  string `json:"Brancos11"`
	NumeroDaNotaFiscalDeVnda   string `json:"NumeroDaNotaFiscalDeVnda"`
	Brancos12                  string `json:"Brancos12"`
	NumeroDaNotaFiscalDeOrigem string `json:"NumeroDaNotaFiscalDeOrigem"`
	Brancos13                  string `json:"Brancos13"`
	FimDeRegistro              string `json:"FimDeRegistro"`
}

func (d *DevolucaoDeProdutos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDevolucaoDeProdutos

	err = posicaoParaValor.ReturnByType(&d.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoDoVendedor, "CodigoDoVendedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Branco0, "Branco0")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroDaNotaFiscal0, "NumeroDaNotaFiscal0")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Branco1, "Branco1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SerieDaNotaFiscal, "SerieDaNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Branco2, "Branco2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DataDaNotaFiscal, "DataDaNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Branco3, "Branco3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CnpjDoCliente, "CnpjDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Branco4, "Branco4")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoDoproduto, "CodigoDoproduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Branco5, "Branco5")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Branco6, "Branco6")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorUnitario, "ValorUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Branco7, "Branco7")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorTotal, "ValorTotal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Brancos8, "Brancos8")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoDeBarrasDoProduto, "CodigoDeBarrasDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Brancos9, "Brancos9")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NotaFiscalDeOrigem, "NotaFiscalDeOrigem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Brancos10, "Brancos10")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SerieDaNotaFiscalDeOrigem, "SerieDaNotaFiscalDeOrigem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Brancos11, "Brancos11")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroDaNotaFiscalDeVnda, "NumeroDaNotaFiscalDeVnda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Brancos12, "Brancos12")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroDaNotaFiscalDeOrigem, "NumeroDaNotaFiscalDeOrigem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Brancos13, "Brancos13")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.FimDeRegistro, "FimDeRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDevolucaoDeProdutos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":             {0, 1, 0},
	"CodigoDoVendedor":           {1, 7, 0},
	"Branco0":                    {7, 8, 0},
	"NumeroDaNotaFiscal0":        {8, 14, 0},
	"Branco1":                    {14, 15, 0},
	"SerieDaNotaFiscal":          {15, 18, 0},
	"Branco2":                    {18, 19, 0},
	"DataDaNotaFiscal":           {19, 27, 0},
	"Branco3":                    {27, 28, 0},
	"CnpjDoCliente":              {28, 42, 0},
	"Branco4":                    {42, 43, 0},
	"CodigoDoproduto":            {43, 49, 0},
	"Branco5":                    {49, 50, 0},
	"Quantidade":                 {50, 67, 0},
	"Branco6":                    {67, 68, 0},
	"ValorUnitario":              {68, 85, 0},
	"Branco7":                    {85, 86, 0},
	"ValorTotal":                 {86, 103, 0},
	"Brancos8":                   {103, 104, 0},
	"CodigoDeBarrasDoProduto":    {104, 117, 0},
	"Brancos9":                   {117, 118, 0},
	"NotaFiscalDeOrigem":         {118, 124, 0},
	"Brancos10":                  {124, 125, 0},
	"SerieDaNotaFiscalDeOrigem":  {125, 128, 0},
	"Brancos11":                  {128, 129, 0},
	"NumeroDaNotaFiscalDeVnda":   {129, 138, 0},
	"Brancos12":                  {138, 139, 0},
	"NumeroDaNotaFiscalDeOrigem": {139, 148, 0},
	"Brancos13":                  {148, 599, 0},
	"FimDeRegistro":              {599, 600, 0},
}
