package mapaDeVendas

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type VendasRealizadas struct {
	TipoDoRegistro             string	`json:"TipoDoRegistro"`
	CodigoDoVendedor           string	`json:"CodigoDoVendedor"`
	Branco0                    string	`json:"Branco0"`
	NumeroDaNotaFiscalDeVenda0 string	`json:"NumeroDaNotaFiscalDeVenda0"`
	Branco1                    string	`json:"Branco1"`
	SerieDaNotaFiscalDeVenda   string	`json:"SerieDaNotaFiscalDeVenda"`
	Branco2                    string	`json:"Branco2"`
	DataDaNotaFiscalDeVenda    string	`json:"DataDaNotaFiscalDeVenda"`
	Branco3                    string	`json:"Branco3"`
	CnpjDoCliente              string	`json:"CnpjDoCliente"`
	Branco4                    string	`json:"Branco4"`
	CodigoDoProduto            string	`json:"CodigoDoProduto"`
	Branco5                    string	`json:"Branco5"`
	QuantidadeVendida          int64 	`json:"QuantidadeVendida"`
	Branco6                    string	`json:"Branco6"`
	ValorUnitarioDoItem        int64 	`json:"ValorUnitarioDoItem"`
	Branco7                    string	`json:"Branco7"`
	ValorTotalDoItem           int64 	`json:"ValorTotalDoItem"`
	Branco8                    string	`json:"Branco8"`
	NumeroDoPedidoOriginal     string	`json:"NumeroDoPedidoOriginal"`
	Branco9                    string	`json:"Branco9"`
	CodigoDeBarrasDoProduto    string	`json:"CodigoDeBarrasDoProduto"`
	Branco10                   string	`json:"Branco10"`
	NumeroDaNotaFiscalDeVenda1 string	`json:"NumeroDaNotaFiscalDeVenda1"`
	Branco11                   string	`json:"Branco11"`
	FimDeRegistro              string	`json:"FimDeRegistro"`
}

func (v *VendasRealizadas) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesVendasRealizadas

	err = posicaoParaValor.ReturnByType(&v.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.CodigoDoVendedor, "CodigoDoVendedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco0, "Branco0")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.NumeroDaNotaFiscalDeVenda0, "NumeroDaNotaFiscalDeVenda0")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco1, "Branco1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.SerieDaNotaFiscalDeVenda, "SerieDaNotaFiscalDeVenda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco2, "Branco2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.DataDaNotaFiscalDeVenda, "DataDaNotaFiscalDeVenda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco3, "Branco3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.CnpjDoCliente, "CnpjDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco4, "Branco4")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.CodigoDoProduto, "CodigoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco5, "Branco5")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.QuantidadeVendida, "QuantidadeVendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco6, "Branco6")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.ValorUnitarioDoItem, "ValorUnitarioDoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco7, "Branco7")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.ValorTotalDoItem, "ValorTotalDoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco8, "Branco8")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.NumeroDoPedidoOriginal, "NumeroDoPedidoOriginal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco9, "Branco9")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.CodigoDeBarrasDoProduto, "CodigoDeBarrasDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco10, "Branco10")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.NumeroDaNotaFiscalDeVenda1, "NumeroDaNotaFiscalDeVenda1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.Branco11, "Branco11")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&v.FimDeRegistro, "FimDeRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesVendasRealizadas = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 1, 0},
	"CodigoDoVendedor":                      {1, 7, 0},
	"Branco0":                      {7, 8, 0},
	"NumeroDaNotaFiscalDeVenda0":                      {8, 14, 0},
	"Branco1":                      {14, 15, 0},
	"SerieDaNotaFiscalDeVenda":                      {15, 18, 0},
	"Branco2":                      {18, 19, 0},
	"DataDaNotaFiscalDeVenda":                      {19, 27, 0},
	"Branco3":                      {27, 28, 0},
	"CnpjDoCliente":                      {28, 42, 0},
	"Branco4":                      {42, 43, 0},
	"CodigoDoProduto":                      {43, 49, 0},
	"Branco5":                      {49, 50, 0},
	"QuantidadeVendida":                      {50, 67, 0},
	"Branco6":                      {67, 68, 0},
	"ValorUnitarioDoItem":                      {68, 85, 0},
	"Branco7":                      {85, 86, 0},
	"ValorTotalDoItem":                      {86, 103, 0},
	"Branco8":                      {103, 104, 0},
	"NumeroDoPedidoOriginal":                      {104, 113, 0},
	"Branco9":                      {113, 114, 0},
	"CodigoDeBarrasDoProduto":                      {114, 127, 0},
	"Branco10":                      {127, 128, 0},
	"NumeroDaNotaFiscalDeVenda1":                      {128, 137, 0},
	"Branco11":                      {137, 599, 0},
	"FimDeRegistro":                      {599, 600, 0},
}