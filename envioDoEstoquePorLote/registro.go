package envioDoEstoquePorLote

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Registro struct {
	CnpjOperadorLogistico string `json:"CnpjOperadorLogistico"`
	CodigoEanDoProduto    string `json:"CodigoEanDoProduto"`
	Lote                  string `json:"Lote"`
	Estoque               int64  `json:"Estoque"`
	Data                  string `json:"Data"`
}

func (r *Registro) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistro

	err = posicaoParaValor.ReturnByType(&r.CnpjOperadorLogistico, "CnpjOperadorLogistico")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoEanDoProduto, "CodigoEanDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Lote, "Lote")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Estoque, "Estoque")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Data, "Data")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistro = map[string]gerador_layouts_posicoes.Posicao{
	"CnpjOperadorLogistico": {0, 14, 0},
	"CodigoEanDoProduto":    {14, 27, 0},
	"Lote":                  {27, 37, 0},
	"Estoque":               {37, 54, 0},
	"Data":                  {54, 62, 0},
}
